      </div>
      <footer id="footer" class="site-footer" role="contentinfo">
        <div class="content-container">
          <div id="copyright">
          &copy; <?php echo esc_html( date_i18n( __( 'Y', 'blankslate' ) ) ); ?> <?php echo esc_html( get_bloginfo( 'name' ) ); ?>
          </div>
        </div>
      </footer>
    </div>
    <?php wp_footer(); ?>
  </body>
</html>