<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo('charset'); ?>" />
  <meta name="viewport" content="width=device-width" />
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
  <div id="wrapper" class="hfeed">
    <header id="header" class="site-header" role="banner">
      <div class="content-container">
        <div id="branding">
          <div id="site-title" itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
            <?php if (is_front_page() || is_home() || is_front_page() && is_home()) {
              echo '<h1>';
            } ?>
            <?php
            if (function_exists('the_custom_logo')) {
              the_custom_logo();
            } else { ?>
              <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name')); ?>" rel="home" itemprop="url"><span itemprop="name"><?php esc_html( get_bloginfo( 'name' ) ); ?></span></a>
            <?php } ?>
            <?php if (is_front_page() || is_home() || is_front_page() && is_home()) {
              echo '</h1>';
            } ?>
          </div>
          <div id="site-description"><?php bloginfo('description'); ?></div>
        </div>
        <nav id="menu" role="navigation" itemscope itemtype="https://schema.org/SiteNavigationElement">
          <div id="search"><?php get_search_form(); ?></div>
          <?php wp_nav_menu( array( 'theme_location' => 'main-menu', 'link_before' => '<span itemprop="name">', 'link_after' => '</span>' ) ); 
          ?>
        </nav>
      </div>
    </header>
    <div id="container" class="content-container">